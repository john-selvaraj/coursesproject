jQuery(document).ready(function() {

	jQuery('.catalog_switch .btn').click(function(event){
		event.preventDefault();

		if(!jQuery(this).hasClass('active')) {
			var t = jQuery(this).data('target');
			var o = jQuery(this).data('other');
			jQuery('.'+o).hide();
			jQuery('.'+t).show();
			jQuery('.catalog_switch .btn').removeClass('active');
			jQuery(this).addClass('active');
		}

	});


	jQuery('.fakeinput_password a.show').click(function(event){
		event.preventDefault();
		jQuery(this).find('.vizoff').toggle();
		jQuery(this).find('.vizon').toggle();
		var x = document.getElementById("login_pswd_input");
	    if (x.type === "password") {
	        x.type = "text";
	    } else {
	        x.type = "password";
	    }
	});

});