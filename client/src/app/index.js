import React from "react";
import ReactDOM from "react-dom";

import Header from "./components/Header";
import SponsoredCourses from "./components/SponsoredCourses";
import Home from "./components/Home";
import Footer from "./components/Footer";

class App extends React.Component{
    render(){
        return(
            <div id="container">
                <header className="header">
                    <Header/>
                </header>
                <div id="sponsored_container">
                    <SponsoredCourses/>
                </div>
                <main role="main">
                    <Home/>
                </main>
                <footer className="footer">
                    <Footer/>
                </footer>
            </div>
        );
    }
}

ReactDOM.render(<App/>,document.getElementById("app"));