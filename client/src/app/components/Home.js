import React, { Component } from "react";

class Home extends Component{
    render(){
        return(
            <div className="container browse_catalog">
                <div className="row">
                    <div className="col-sm-24 text-center">
                        <h2>Browse Courses Catalog</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-24 text-center catalog_switch">
                        <div className="btn-group" role="group" aria-label="Courses Catalog">
                            <button type="button" className="btn active" data-target="list_by_category" data-other="list_by_company">By Category</button>
                            <button type="button" className="btn" data-target="list_by_company" data-other="list_by_category">By Company</button>
                        </div>
                    </div>
                </div>
                <div className="row list_by_category">
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/cat1.png" className="cover mw-100" />
                            <span className="name">Air Traffic Control</span>
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/cat2.png" className="cover mw-100" />
                            <span className="name">Pilot Training</span>
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/cat1.png" className="cover mw-100" />
                            <span className="name">Flight Instructor</span>
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/cat2.png" className="cover mw-100" />
                            <span className="name">Helicopter Training</span>
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/cat1.png" className="cover mw-100" />
                            <span className="name">Aircraft Maintenance</span>
                        </a>
                    </div>
                </div>
                <div className="row list_by_company" style={{display:'none'}}>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo1.png" className="mw-100" />
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo2.png" className="mw-100" />
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo1.png" className="mw-100" />
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo2.png" className="mw-100" />
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo2.png" className="mw-100" />
                        </a>
                    </div>
                    <div className="col-12 col-md-6">
                        <a href="#" className="item">
                            <img src="images/complogo1.png" className="mw-100" />
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home