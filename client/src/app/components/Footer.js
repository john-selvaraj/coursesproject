import React, { Component } from "react";

class Footer extends Component{
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-8 text-center text-md-left">
                        <img src="assets/logo/white-sq.svg" className="logo" />
                    </div>
                    <div className="col-md-9 col-lg-11 d-none d-md-flex"></div>
                <div className="col-md-7 col-lg-5 social">
                    <div className="text-white d-none d-md-block">Follow us on</div>
                    <ul className="links d-flex justify-content-around justify-content-md-between">
                    <li><a href="#"><img src="assets/social/facebook.svg" /></a></li>
                    <li><a href="#"><img src="assets/social/googleplus.svg" /></a></li>
                    <li><a href="#"><img src="assets/social/linkedin.svg" /></a></li>
                    </ul>
                </div>
                </div>
            </div>
        );
    }
}

export default Footer