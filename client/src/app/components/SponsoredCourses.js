import React, { Component } from "react";

import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faMapMarkerAlt,faPoundSign } from '@fortawesome/free-solid-svg-icons';

class SponsoredCourses extends Component {

    render(){
        return (
            <div id="sponsored-rows">
                <div className="sponsored d-none d-md-block">
                    <div className="container d-none d-lg-block">
                        <div id="carouselSponsored" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course over two lines</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdfasdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdf asdf asdf asdf asdf asdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdfasdfasdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-md-6">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdf asdf asdf asdf </div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselSponsored" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselSponsored" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div className="container d-none d-md-block d-lg-none">
                        <div id="carouselSponsoredTablet" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="row">
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course over two lines</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="row">
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdfasdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdf asdf asdf asdf asdf asdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                        <div className="col-8">
                                            <a href="#" className="item">
                                                <img className="logo mw-100" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">asdfasdfasdf</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselSponsoredTablet" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselSponsoredTablet" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-24 text-right footnote">Sponsored courses</div>
                        </div>
                    </div>
                </div>
                <div className="sponsored mobile d-block d-md-none">
                    <div className="container">
                        <div id="carouselSponsoredMobile" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div className="row">
                                        <div className="col-24">
                                            <a href="#" className="item">
                                                <img className="logo" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="row">
                                        <div className="col-24">
                                            <a href="#" className="item">
                                                <img className="logo" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course over two lines</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="row">
                                        <div className="col-24">
                                            <a href="#" className="item">
                                                <img className="logo" src="images/complogo1.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div className="row">
                                        <div className="col-24">
                                            <a href="#" className="item">
                                                <img className="logo" src="images/complogo2.png" />
                                                <img className="star" src="assets/icons/Save.svg" />
                                                <div className="title">Title of the course</div>
                                                <div className="location">Liverpool John Lennon Airport</div>
                                                <div className="type">Beginner | Apprenticeship</div>
                                                <div className="price">&pound;1,234</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#carouselSponsoredMobile" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next" href="#carouselSponsoredMobile" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-24 text-right footnote">Sponsored courses</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SponsoredCourses