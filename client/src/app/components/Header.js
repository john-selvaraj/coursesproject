import React, { Component } from "react";

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    InputGroup, 
    InputGroupAddon, 
    InputGroupText, 
    Input,
    Button } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faMapMarkerAlt,faPoundSign } from '@fortawesome/free-solid-svg-icons';

class Header extends Component{

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render(){
        return(
            <div className="container">
                <div className="row toprow">
                    <div className="col-12 col-sm-12 col-md-24 col-lg-6 text-md-center">
                        <img src="assets/logo/white-ls.svg" className="logo mw-100 d-none d-md-inline" />
                        <img src="assets/logo/white-sq.svg" className="logo mw-100 d-md-none" />
                    </div>
                    <div className="col-12 col-sm-12 col-md-18 col-lg-14">
                        <nav className="navbar navbar-expand-md justify-content-end justify-content-md-between">
                        <button className="navbar-toggler" type="button" data-toggle="modal" data-target="#mobileNavModal">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                            <ul className="navbar-nav navigation d-flex justify-content-between justify-content-lg-end">
                                <li className="nav-item">
                                    <a href="#" className="text-white">Aviation Job Search</a>
                                </li>
                                <li className="nav-item">
                                    <a href="#" className="text-white">New Courses</a>
                                </li>
                                <li className="nav-item">
                                    <a href="#" className="text-white">Browse<span className="d-none d-lg-inline"> Categories</span></a>
                                </li>
                                <li className="nav-item text-white">
                                    <a href="#" className="text-white">Register</a> | <a href="#" className="text-white" data-toggle="modal" data-target="#loginModal">Login</a>
                                </li>
                            </ul>
                        </div>
                        </nav>
                    </div>
                    <div className="col-md-6 col-lg-4 d-none d-md-flex">
                        <a href="#" className="btn w-100 border border-white rounded text-white post_course">Post a course</a>
                    </div>
                </div>
                <div className="row hero">
                    <div className="col-md-12 col-lg-16 text-center text-md-left">
                        <h1 className="text-white">What do you want to learn?</h1>
                    </div>
                    <div className="col-md-12 col-lg-8 search">
                        <input type="text" name="" className="special keyword" placeholder="I am looking for..." />
                        <input type="text" name="" className="special location" placeholder="located in..." />
                        <a href="#" className="btn special">Search</a>
                    </div>
                </div>
                <div className="modal main_nav_modal" id="mobileNavModal" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <img src="assets/logo/white-sq.svg" className="logo" />
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><img src="assets/icons/mobile-close-white.svg" /></span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <ul className="mob_nav">
                                    <li><a href="#">Aviation Job Search</a></li>
                                    <li><a href="#">New Courses</a></li>
                                    <li><a href="#">Browse Categories</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#loginModal">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Header