const path              = require('path');
const webpack           = require('webpack');
const htmlPlugin        = require('html-webpack-plugin');
const openBrowserPlugin = require('open-browser-webpack-plugin'); 
const dashboardPlugin   = require('webpack-dashboard/plugin');
const autoprefixer      = require('autoprefixer');

const PATHS = {
  app: path.resolve(__dirname, 'src'),
  images:path.resolve(__dirname,'src/assets/'),
  build: path.resolve(__dirname, 'dist')
};

module.exports = {
  entry: PATHS.app+"/app/index.js",
  output: {
    path: PATHS.build+"/app",
    filename: 'bundle.js',
    publicPath: "/app/"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        include: PATHS.app,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
          presets: ["react","es2015","stage-0"]
        }
      }     
    ]
  }
};