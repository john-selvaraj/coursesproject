import React, { Component } from "react";

import { TabContent, TabPane, Nav, NavItem, NavLink, Button, Card, CardBody, CardTitle, CardSubtitle, CardText, CardImg, CardImgOverlay,Row, Col } from 'reactstrap';

import classnames from 'classnames';

class Home extends Component{
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1'
        };
    }
    
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render(){
        return(
            <div className="container-fluid bg-3 courseCatalog">
                <h3 align="center">Browse Course Catalog</h3>
                <div className="row">
                    <div className="col-md-12">
                        <Nav tabs>
                            <NavItem className="leftTab">
                                <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                                >
                                By Category
                                </NavLink>
                            </NavItem>
                            <NavItem className="rightTab">
                                <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                                >
                                By Company
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" top height="100%" src="assets/air-traffic.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Air Traffic Control</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" top height="100%" src="assets/pilot-training.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Pilot Training</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/flight-instructor.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Flight Instructor</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/helicopter-training.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Helicopter Training</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/helicopter-training.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Helicopter Training</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/flight-instructor.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Flight Instructor</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/pilot-training.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Pilot Training</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                    <Col sm="3">
                                        <Card inverse>
                                            <CardImg width="100%" src="assets/air-traffic.jpg" alt="Card image cap" />
                                            <CardImgOverlay>
                                                <CardTitle>Air Traffic Control</CardTitle>
                                            </CardImgOverlay>
                                        </Card>
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="2">
                                <Row>
                                    <Col sm="6">
                                        <Card body>
                                        <CardTitle>Special Title Treatment</CardTitle>
                                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                        <Button>Go somewhere</Button>
                                        </Card>
                                    </Col>
                                    <Col sm="6">
                                        <Card body>
                                        <CardTitle>Special Title Treatment</CardTitle>
                                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                        <Button>Go somewhere</Button>
                                        </Card>
                                    </Col>
                                </Row>
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home