import React, { Component } from "react";

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    InputGroup, 
    InputGroupAddon, 
    InputGroupText, 
    Input,
    Button } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faMapMarkerAlt,faPoundSign } from '@fortawesome/free-solid-svg-icons';

class Header extends Component{

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render(){
        return(
            <div className="mainMenu">
                <Navbar color="light" light expand="md">
                <NavbarBrand href="/">Aviation Courses</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="http://www.aviationjobsearch.com/">Aviation Jobs Search</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://www.aviationjobsearch.com/courses">New Courses</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://www.aviationjobsearch.com/courses">Browse Categories</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://www.aviationjobsearch.com/recruiters/log-in">Register | Login</NavLink>
                        </NavItem>
                        <NavItem>
                            <Button className="post-course-btn">Post a course</Button>
                        </NavItem>
                    </Nav>
                </Collapse>
                </Navbar>
                <div className="jumbotron">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 text-left">
                                <h1>What do you<br/>      
                                want to learn?</h1>
                            </div>
                            <div className="col-md-6 text-center search-container">
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        @
                                    </InputGroupAddon>
                                    <Input placeholder="I am looking for..." />
                                </InputGroup>
                                <br />
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        @
                                    </InputGroupAddon>
                                    <Input placeholder="located in..." />
                                </InputGroup>
                                <br/>
                                <Button className="search-btn">Search</Button>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }

}

export default Header