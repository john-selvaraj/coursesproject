import React, { Component } from "react";

class Footer extends Component{
    render(){
        return(
            <div className="footer">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-6">
                            Aviation Search Logo
                        </div>
                        <div className="col-md-6 text-right">
                            Aviation Search Social Links
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer