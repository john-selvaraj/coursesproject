import React, { Component } from "react";

import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faMapMarkerAlt,faPoundSign } from '@fortawesome/free-solid-svg-icons';

class SponsoredCourses extends Component {

    render(){
        return (
            <div className="container-fluid bg-3 sponsoredContainer">
                <div className="row">
                    <div className="col-md-3">
                        <Card>
                            <CardImg top width="100%" src="https://www.aviationjobsearch.com/uploads/company_logo/thumbnail/370x273/fdaa5b4df3e4a943b95b933b4b622112092a8d46.jpg" alt="Card image cap" />
                            <CardBody>
                            <CardTitle>Title of the course</CardTitle>
                            <CardSubtitle>
                                <FontAwesomeIcon
                                    icon={faMapMarkerAlt}
                                    color="#2B6FC3"
                                    size="lg"
                                />
                                {' '} Liverpool John Lennon Airport
                            </CardSubtitle>
                            <h5 className="course-level">Beginner | Apprenticeship</h5>
                            <h6 className="course-price"><FontAwesomeIcon
                                icon={faPoundSign}
                                color="#2B6FC3"
                                size="lg"
                            />
                            {' '} 1,240</h6>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-md-3">
                        <Card>
                            <CardImg top width="100%" src="https://www.aviationjobsearch.com/uploads/company_logo/thumbnail/370x273/fcbd25b4f600f38b16914a344e6d95735d711bfc.jpg" alt="Card image cap" />
                            <CardBody>
                            <CardTitle>Title of the course</CardTitle>
                            <CardSubtitle>
                                <FontAwesomeIcon
                                    icon={faMapMarkerAlt}
                                    color="#2B6FC3"
                                    size="lg"
                                />
                                {' '} Liverpool John Lennon Airport
                            </CardSubtitle>
                            <h5 className="course-level">Beginner | Apprenticeship</h5>
                            <h6 className="course-price"><FontAwesomeIcon
                                icon={faPoundSign}
                                color="#2B6FC3"
                                size="lg"
                            />
                            {' '} 1,240</h6>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-md-3">
                        <Card>
                            <CardImg top width="100%" src="https://www.aviationjobsearch.com/uploads/company_logo/thumbnail/370x273/062050ac4b6bd0bea470cadeb209c1692a8302c7.jpg" alt="Card image cap" />
                            <CardBody>
                            <CardTitle>Title of the course</CardTitle>
                            <CardSubtitle>
                                <FontAwesomeIcon
                                    icon={faMapMarkerAlt}
                                    color="#2B6FC3"
                                    size="lg"
                                />
                                {' '} Liverpool John Lennon Airport
                            </CardSubtitle>
                            <h5 className="course-level">Beginner | Apprenticeship</h5>
                            <h6 className="course-price"><FontAwesomeIcon
                                icon={faPoundSign}
                                color="#2B6FC3"
                                size="lg"
                            />
                            {' '} 1,240</h6>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-md-3">
                        <Card>
                            <CardImg top width="100%" src="https://www.aviationjobsearch.com/uploads/company_logo/thumbnail/370x273/098c0f2144084439c06397ac2afbae0256b94553.jpg" alt="Card image cap" />
                            <CardBody>
                            <CardTitle>Title of the course</CardTitle>
                            <CardSubtitle>
                                <FontAwesomeIcon
                                    icon={faMapMarkerAlt}
                                    color="#2B6FC3"
                                    size="lg"
                                />
                                {' '} Liverpool John Lennon Airport
                            </CardSubtitle>
                            <h5 className="course-level">Beginner | Apprenticeship</h5>
                            <h6 className="course-price"><FontAwesomeIcon
                                icon={faPoundSign}
                                color="#2B6FC3"
                                size="lg"
                            />
                            {' '} 1,240</h6>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}

export default SponsoredCourses