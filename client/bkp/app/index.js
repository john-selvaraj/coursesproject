import React from "react";
import ReactDOM from "react-dom";

import Header from "./components/Header";
import SponsoredCourses from "./components/SponsoredCourses";
import Home from "./components/Home";
import Footer from "./components/Footer";

class App extends React.Component{
    render(){
        return(
            <div className="container-fluid">
                <div className="row blue-bg">
                    <div className="col-md-12">
                        <Header/>
                    </div>
                </div>
                <div className="row lightblue-bg">
                    <div className="col-md-12">
                        <SponsoredCourses/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <Home/>
                    </div>
                </div>
                <div className="row blue-bg">
                    <div className="col-md-12">
                        <Footer/>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App/>,document.getElementById("app"));