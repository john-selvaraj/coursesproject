import React,{Component} from 'react';
import RichTextInput from 'ra-input-rich-text';
import { AdminBuilder, hydraClient } from '@api-platform/admin';
import parseHydraDocumentation from '@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation';

const entrypoint = process.env.REACT_APP_API_ENTRYPOINT;

export default class extends Component {
  state = { api: null };

  componentDidMount() {

    parseHydraDocumentation(entrypoint).then(({ api }) => {
      const courses = api.resources.find(({ name }) => 'courses' === name);

      const description = courses.fields.find(f => 'description' === f.name);

      description.input = props => (
        <RichTextInput {...props} source={description.name} />
      );

      description.input.defaultProps = {
        addField: true,
        addLabel: true
      };

      this.setState({ api });
    });
    
  }

  render() {
    if (null === this.state.api) return <div>Loading...</div>;

    return <AdminBuilder api={ this.state.api } dataProvider={ hydraClient(this.state.api) }/>
  }
}
