<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	$course	= new Course();
	    $course->setName('Dispatcher of Aviation Course');
        $course->setDateOfCourse('26/11/2018');
        $course->setDuration('1 week');
        $course->setPrice('200 £');
        $course->setMethodOfStudy('Online');
        $course->setCourseLevel('Beginner');
        $course->setCategory('Air Traffic Control');
        $course->setTakePlace('Online');
        $course->setLocations('UK');
        $course->setDescription('The Aircraft Dispatcher is a licensed airman certificated by a Civil Aviation Administration. He/She has joint responsibility with the captain for the safety and operational control of flights under his/her guidance. To achieve best economics practices, passengers service and operational control, the Flight dispatcher analyses and evaluates meteorological information to determine potential hazards to safety of flight and to select the most desirable and economical route of flight, computes the amount of fuel required for the safe completion of flight according to type of aircraft, distance of flight, maintenance, limitations, weather conditions and minimum fuel requirements prescribed by aviation regulations.');
        $course->setImages('');
        $course->setVideos('');
        $course->setStatus('Active');
        $manager->persist($course);

        $course	= new Course();
	    $course->setName('Instrumental Rating Course on Multi-engines Helicopter');
        $course->setDateOfCourse('26/11/2018');
        $course->setDuration('2 weeks');
        $course->setPrice('150 £');
        $course->setMethodOfStudy('Online');
        $course->setCourseLevel('Beginner');
        $course->setCategory('Air Traffic Control');
        $course->setTakePlace('Online');
        $course->setLocations('UK');
        $course->setDescription('This rating can be attended by a private pilot who holds night rating with at least 50 hours of cross-country or a pilot who is training for a CPL modular course, an integrated ATPL course or a pilot who already holds a CPL license.');
        $course->setImages('');
        $course->setVideos('');
        $course->setStatus('Active');
        $manager->persist($course);


        $course	= new Course();
	    $course->setName('From 0 to CPL with ATPL(frozen)');
        $course->setDateOfCourse('26/11/2018');
        $course->setDuration('1 week');
        $course->setPrice('100 £');
        $course->setMethodOfStudy('Online');
        $course->setCourseLevel('Beginner');
        $course->setCategory('Air Traffic Control');
        $course->setTakePlace('Online');
        $course->setLocations('UK');
        $course->setDescription('What you get? - CPL - Commercial pilot licence with – (IR) Instrument rating, (ME) Multi engine rating, "Frozen" ATPL - Airline transport pilot license (theoretical course) with minimum of 200 flight hour.');
        $course->setImages('');
        $course->setVideos('');
        $course->setStatus('Active');
        $manager->persist($course);

        $user = new User();
        $user->setFullName('John S');
        $user->setUsername('john.selvaraj@fridaymediagroup.com');
        $user->setEmail('john.selvaraj@fridaymediagroup.com');
        $user->setPassword('123456');
        $user->setLocation('Bangalore');
        $user->setGplusToken('');
        $user->setLinkedinToken('');
        $user->setRole('SuperAdmin');
        $user->setStatus('Active');
        $manager->persist($user);

        $user = new User();
        $user->setFullName('Salamon S');
        $user->setUsername('john.selvaraj@fridaymediagroup.com');
        $user->setEmail('john.selvaraj@fridaymediagroup.com');
        $user->setPassword('123456');
        $user->setLocation('Bangalore');
        $user->setGplusToken('');
        $user->setLinkedinToken('');
        $user->setRole('Admin');
        $user->setStatus('Active');
        $manager->persist($user);

        $user = new User();
        $user->setFullName('Peter S');
        $user->setUsername('john.selvaraj@fridaymediagroup.com');
        $user->setEmail('john.selvaraj@fridaymediagroup.com');
        $user->setPassword('123456');
        $user->setLocation('Bangalore');
        $user->setGplusToken('');
        $user->setLinkedinToken('');
        $user->setRole('User');
        $user->setStatus('Active');
        $manager->persist($user);

        $manager->flush();
    }
}
