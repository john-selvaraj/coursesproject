<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read","admin:read"}},
 *     denormalizationContext={"groups"={"write","admin:write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table("app_user")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $full_name;

     /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gplus_token;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedin_token;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @Groups({"read","write","admin:write","admin:read"})
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $emailCanonical;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $usernameCanonical;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    public function setFullName(string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->$username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getGplusToken(): ?string
    {
        return $this->gplus_token;
    }

    public function setGplusToken(?string $gplus_token): self
    {
        $this->gplus_token = $gplus_token;

        return $this;
    }

    public function getLinkedinToken(): ?string
    {
        return $this->linkedin_token;
    }

    public function setLinkedinToken(?string $linkedin_token): self
    {
        $this->linkedin_token = $linkedin_token;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    public function setEmailCanonical(string $emailCanonical): self
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical(string $usernameCanonical): self
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }
}
